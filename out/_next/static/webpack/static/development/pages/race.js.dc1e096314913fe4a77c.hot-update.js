webpackHotUpdate("static/development/pages/race.js",{

/***/ "./pages/race.js":
/*!***********************!*\
  !*** ./pages/race.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/json/stringify */ "./node_modules/@babel/runtime-corejs2/core-js/json/stringify.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/core-js/object/assign */ "./node_modules/next/dist/build/polyfills/object-assign.js");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime-corejs2/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime-corejs2/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/assertThisInitialized */ "./node_modules/@babel/runtime-corejs2/helpers/esm/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/inherits */ "./node_modules/@babel/runtime-corejs2/helpers/esm/inherits.js");
/* harmony import */ var _components_MyLayout__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/MyLayout */ "./components/MyLayout.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! isomorphic-unfetch */ "./node_modules/next/dist/build/polyfills/fetch/index.js");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_11__);









var _jsxFileName = "/mnt/r/myTemple/irvin-challenge/pages/race.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_10___default.a.createElement;



var cardStyle = {
  width: '9rem'
};
var imgStyle = {
  maxWidth: '8rem',
  maxHeight: '8rem',
  width: 'auto',
  height: 'auto'
};
var extraCentered = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  justifyContent: 'center',
  alignItems: 'center'
};

var Race =
/*#__PURE__*/
function (_Component) {
  Object(_babel_runtime_corejs2_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_8__["default"])(Race, _Component);

  function Race(props) {
    var _this;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Race);

    _this = Object(_babel_runtime_corejs2_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__["default"])(this, Object(_babel_runtime_corejs2_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__["default"])(Race).call(this, props));
    _this.state = {
      keyword: null,
      liveImages: null,
      currentImages: null,
      sellers: null,
      race: null,
      winner: null,
      totalPoints: null,
      raceIsEnded: false,
      pagination: 1,
      imagesBUrl: 'https://www.googleapis.com/customsearch/v1?key=AIzaSyDV5WxK2waZ-h7VFxB3egdWLaOVgvVqH5E&cx=010845818822579063968:fyfmqbqbcp4&searchType=image',
      alegraBUrl: 'https://api.alegra.com/api/v1/',
      alegraToken: 'Basic aXJ2aW4uamFpci5wZ0BnbWFpbC5jb206MWRhZmJiN2I5ZTM4MmZjYTA3NDM='
    };
    _this.randomImageAndRest = _this.randomImageAndRest.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.liveImagesAndCurrentImages = _this.liveImagesAndCurrentImages.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.handleKeyword = _this.handleKeyword.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.renderLiveSellers = _this.renderLiveSellers.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.createInvoice = _this.createInvoice.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.handleNewRace = _this.handleNewRace.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.getImages = _this.getImages.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.handleSearch = _this.handleSearch.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.handleMoreOpts = _this.handleMoreOpts.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.handleRaceAndWinner = _this.handleRaceAndWinner.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    _this.handlePurchase = _this.handlePurchase.bind(Object(_babel_runtime_corejs2_helpers_esm_assertThisInitialized__WEBPACK_IMPORTED_MODULE_7__["default"])(_this));
    return _this;
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(Race, [{
    key: "randomImageAndRest",
    value: function randomImageAndRest(arr) {
      var randIdx = Math.round(Math.random() * (arr.length - 1));
      var randomImage = arr[randIdx];
      var arrayRest = arr.filter(function (el, idx) {
        return idx != randIdx;
      });
      return {
        randomImage: randomImage,
        arrayRest: arrayRest
      };
    }
  }, {
    key: "liveImagesAndCurrentImages",
    value: function liveImagesAndCurrentImages(arr) {
      var currentImages = arr;
      var liveImages = [];
      var sellers = this.state.sellers;
      var numOfSellers = sellers.length;

      for (var i = 0; i < numOfSellers; i++) {
        var _this$randomImageAndR = this.randomImageAndRest(currentImages),
            randomImage = _this$randomImageAndR.randomImage,
            arrayRest = _this$randomImageAndR.arrayRest;

        liveImages.push(_babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_2___default()(randomImage, sellers[i]));
        currentImages = arrayRest;
      }

      return {
        liveImages: liveImages,
        currentImages: currentImages
      };
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var url, urlObject, q, getSellers;
      return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function componentDidMount$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              url = window.location;
              urlObject = new URL(url);
              q = urlObject.searchParams.get('q');
              _context.next = 5;
              return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_11___default()(this.state.alegraBUrl + 'sellers/', {
                headers: {
                  'Authorization': this.state.alegraToken
                }
              })["catch"](function (err) {
                return console.error(err);
              }));

            case 5:
              getSellers = _context.sent;
              _context.t0 = this;
              _context.t1 = q;
              _context.next = 10;
              return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(getSellers.json());

            case 10:
              _context.t2 = _context.sent;
              _context.t3 = {
                keyword: _context.t1,
                sellers: _context.t2
              };

              _context.t0.setState.call(_context.t0, _context.t3);

              _context.next = 15;
              return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(this.getImages(this.state.imagesBUrl + "&q=".concat(this.state.keyword)).then(function () {
                _this2.setState({
                  race: _this2.state.sellers.map(function (el) {
                    return {
                      id: el.id,
                      name: el.name,
                      points: 0,
                      restPoints: 20
                    };
                  })
                });
              })["catch"](function (err) {
                return console.error(err);
              }));

            case 15:
            case "end":
              return _context.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "getImages",
    value: function getImages(url) {
      var _this3 = this;

      return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function getImages$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_11___default()(url, {
                method: 'GET'
              }).then(function (res) {
                return res.json();
              }).then(function (res) {
                var _this3$liveImagesAndC = _this3.liveImagesAndCurrentImages(res.items),
                    liveImages = _this3$liveImagesAndC.liveImages,
                    currentImages = _this3$liveImagesAndC.currentImages;

                return {
                  liveImages: liveImages,
                  currentImages: currentImages
                };
              }).then(function (res) {
                _this3.setState({
                  liveImages: res.liveImages,
                  currentImages: res.currentImages
                });
              })["catch"](function (err) {
                return console.error('Error:', err);
              }));

            case 2:
            case "end":
              return _context2.stop();
          }
        }
      });
    }
  }, {
    key: "handleSearch",
    value: function handleSearch() {
      this.setState({
        pagination: 1
      });
      this.getImages(this.state.imagesBUrl + "&q=".concat(this.state.keyword));
      this.setState({
        raceIsEnded: false
      });
      window.history.pushState('', '', "/race?q=".concat(this.state.keyword));
    }
  }, {
    key: "handleMoreOpts",
    value: function handleMoreOpts() {
      var pag = this.state.pagination + 1;
      this.setState({
        pagination: pag
      });
      this.getImages(this.state.imagesBUrl + "&q=".concat(this.state.keyword, "&start=").concat(1 + 10 * (pag - 1)));
    }
  }, {
    key: "handleRaceAndWinner",
    value: function handleRaceAndWinner(id) {
      var updateRace, race, winner, totalPoints;
      return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function handleRaceAndWinner$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              updateRace = this.state.race.map(function (el) {
                if (el.id == id) {
                  el.points = el.points + 3;
                  el.restPoints = 20 - el.points;
                }

                return el;
              });
              race = _babel_runtime_corejs2_core_js_object_assign__WEBPACK_IMPORTED_MODULE_2___default()(this.state.race, updateRace);
              race = race.sort(function (x, y) {
                return x.points < y.points ? 1 : -1;
              });
              this.setState({
                race: race
              });
              winner = race.filter(function (el) {
                return el.points >= 20;
              });

              if (!winner.length) {
                _context3.next = 17;
                break;
              }

              totalPoints = race.map(function (el) {
                return el.points;
              }).reduce(function (x, y) {
                return x + y;
              }, 0);
              _context3.t0 = this;
              _context3.next = 10;
              return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(winner[0]);

            case 10:
              _context3.t1 = _context3.sent;
              _context3.next = 13;
              return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(totalPoints);

            case 13:
              _context3.t2 = _context3.sent;
              _context3.t3 = {
                winner: _context3.t1,
                totalPoints: _context3.t2,
                raceIsEnded: true
              };

              _context3.t0.setState.call(_context3.t0, _context3.t3);

              this.createInvoice();

            case 17:
            case "end":
              return _context3.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "handlePurchase",
    value: function handlePurchase(id) {
      this.handleMoreOpts();
      this.handleRaceAndWinner(id);
    }
  }, {
    key: "handleKeyword",
    value: function handleKeyword(event) {
      event.preventDefault();
      this.setState({
        keyword: event.target.value
      });
    }
  }, {
    key: "handleNewRace",
    value: function handleNewRace() {
      this.setState({
        raceIsEnded: false,
        race: this.state.sellers.map(function (el) {
          return {
            id: el.id,
            name: el.name,
            points: 0,
            restPoints: 20
          };
        })
      });
    }
  }, {
    key: "createInvoice",
    value: function createInvoice() {
      var _this$state, id, totalPoints, invoice;

      return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function createInvoice$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _this$state = this.state, id = _this$state.winner, totalPoints = _this$state.totalPoints;
              invoice = {
                date: new Date().toJSON().split('T')[0],
                dueDate: new Date(new Date() * 1 + 86400000).toJSON().split('T')[0],
                client: 1,
                seller: id,
                items: [{
                  id: 1,
                  price: totalPoints,
                  quantity: 1,
                  description: "WI race's WINNER"
                }]
              };
              _context5.next = 4;
              return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_11___default()(this.state.alegraBUrl + 'invoices/1', {
                method: 'POST',
                body: _babel_runtime_corejs2_core_js_json_stringify__WEBPACK_IMPORTED_MODULE_0___default()(invoice),
                headers: {
                  'Content-Type': 'application/json',
                  'Authorization': this.state.alegraToken
                }
              }).then(function _callee(res) {
                return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.async(function _callee$(_context4) {
                  while (1) {
                    switch (_context4.prev = _context4.next) {
                      case 0:
                        _context4.t0 = console;
                        _context4.next = 3;
                        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.awrap(res.json());

                      case 3:
                        _context4.t1 = _context4.sent;

                        _context4.t0.log.call(_context4.t0, 'invoice res', _context4.t1);

                      case 5:
                      case "end":
                        return _context4.stop();
                    }
                  }
                });
              })["catch"](function (error) {
                return console.error('Error:', error);
              }));

            case 4:
            case "end":
              return _context5.stop();
          }
        }
      }, null, this);
    }
  }, {
    key: "renderLiveSellers",
    value: function renderLiveSellers(sellers) {
      return [].concat(sellers).map(function (seller, i) {
        return __jsx("div", {
          className: "mb-5 text-center",
          style: cardStyle,
          key: seller.id,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 231
          },
          __self: this
        }, __jsx("h5", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 232
          },
          __self: this
        }, i + 1, "\xB0 Place"), seller.points >= 20 ? __jsx("h2", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 234
          },
          __self: this
        }, seller.name, " ", __jsx("span", {
          className: "badge badge-success",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 234
          },
          __self: this
        }, seller.points, " \u2215 20")) : __jsx("h2", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 236
          },
          __self: this
        }, seller.name, " ", __jsx("span", {
          className: "badge badge-info",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 236
          },
          __self: this
        }, seller.points, " \u2215 20")), seller.points >= 20 ? __jsx("h6", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 239
          },
          __self: this
        }, __jsx("span", {
          className: "badge badge-primary",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 239
          },
          __self: this
        }, "W I N N E R")) : __jsx("h6", {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 241
          },
          __self: this
        }, __jsx("span", {
          className: "badge badge-warning",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 241
          },
          __self: this
        }, seller.restPoints, " points to win")));
      });
    }
  }, {
    key: "renderLiveImages",
    value: function renderLiveImages(images) {
      var _this4 = this;

      return [].concat(images).map(function (img, i) {
        return __jsx("div", {
          className: "card text-center m-3",
          key: i,
          style: cardStyle,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 249
          },
          __self: this
        }, __jsx("figure", {
          className: "figure",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 250
          },
          __self: this
        }, __jsx("img", {
          src: img.image.thumbnailLink,
          className: "card-img-top my-1",
          alt: img.title,
          style: imgStyle,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 251
          },
          __self: this
        }), __jsx("figcaption", {
          className: "figure-caption mx-2",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 252
          },
          __self: this
        }, img.title)), __jsx("div", {
          className: "card-body",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 254
          },
          __self: this
        }, __jsx("h6", {
          className: "card-title",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 255
          },
          __self: this
        }, "By ", img.name), __jsx("a", {
          className: "btn btn-primary text-white",
          onClick: function onClick() {
            return _this4.handlePurchase(i + 1);
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 256
          },
          __self: this
        }, "Comprar")));
      });
    }
  }, {
    key: "render",
    value: function render() {
      return __jsx("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 264
        },
        __self: this
      }, __jsx("div", {
        className: "container",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 265
        },
        __self: this
      }, __jsx("div", {
        className: "row my-5",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 266
        },
        __self: this
      }, __jsx("div", {
        className: "col",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 267
        },
        __self: this
      }, __jsx("div", {
        className: "input-group mb-3 align-start",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 268
        },
        __self: this
      }, __jsx("input", {
        type: "text",
        className: "form-control",
        placeholder: "",
        onChange: this.handleKeyword,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 269
        },
        __self: this
      }), __jsx("div", {
        className: "input-group-prepend",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 270
        },
        __self: this
      }, this.state.keyword ? __jsx("button", {
        className: "btn btn-outline-secondary",
        type: "button",
        onClick: this.handleSearch,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 272
        },
        __self: this
      }, "Search") : __jsx("button", {
        className: "btn btn-outline-secondary",
        type: "button",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 274
        },
        __self: this
      }, "Search"))))), __jsx("div", {
        style: extraCentered,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 281
        },
        __self: this
      }, this.state.race && this.renderLiveSellers(this.state.race)), this.state.raceIsEnded ? __jsx("div", {
        className: "row mb-5",
        style: extraCentered,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 286
        },
        __self: this
      }, __jsx("button", {
        type: "button",
        className: "btn btn-success m-5",
        onClick: this.handleNewRace,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 287
        },
        __self: this
      }, "Nueva Carrera >>")) : __jsx("div", {
        className: "row mb-5",
        style: extraCentered,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 290
        },
        __self: this
      }, this.state.liveImages && this.renderLiveImages(this.state.liveImages), __jsx("button", {
        type: "button",
        className: "btn btn-success m-5",
        onClick: this.handleMoreOpts,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 292
        },
        __self: this
      }, "Quiero otras opciones >>"))));
    }
  }]);

  return Race;
}(react__WEBPACK_IMPORTED_MODULE_10__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Object(_components_MyLayout__WEBPACK_IMPORTED_MODULE_9__["default"])(Race));

/***/ })

})
//# sourceMappingURL=race.js.dc1e096314913fe4a77c.hot-update.js.map