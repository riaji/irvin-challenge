import Header from './Header';

const layoutStyle = {
  margin: '10px',
  padding: '0 5px',
  border: '1px solid #DDD',
  minHeight: 'calc(100vh - 20px)'
};

const withLayout = Page => {
  return () => (
    <div style={layoutStyle}>
      <Header />
      <Page />
    </div>
  );
};

export default withLayout;