import Link from 'next/link';
import Head from 'next/head';
import React, { Component } from 'react';
import Modal from 'react-responsive-modal';

class Header extends Component {
  constructor (props) {
    super(props);

    this.state = {
      open: false,
      name: null,
      alegraBUrl: 'https://api.alegra.com/api/v1/',
      alegraToken: 'Basic aXJ2aW4uamFpci5wZ0BnbWFpbC5jb206MWRhZmJiN2I5ZTM4MmZjYTA3NDM='
    };

    this.onOpenModal = this.onOpenModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.handleAddSeller = this.handleAddSeller.bind(this);
    this.handleName = this.handleName.bind(this);
  }

  componentDidMount () {

  }

  onOpenModal () {
    this.setState({ open: true });
  };

  onCloseModal () {
    this.setState({ open: false });
  };

  handleName (e) {
    e.preventDefault();
    let name = e.target.value;
    const test = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/.test(name);

    if (test) {
      name = name.charAt(0).toUpperCase() + name.slice(1);
      this.setState({ name: name });
    }
  }

  async handleAddSeller () {
    const { name, alegraBUrl, alegraToken } = this.state;

    this.onCloseModal();
    this.setState({ name: null });
    
    await fetch(alegraBUrl + 'sellers', {
      method: 'POST',
      body: JSON.stringify({ name: name }),
      headers:{
        'Content-Type': 'application/json',
        'Authorization': alegraToken
      }
    }).then(async res => {
      console.log('invoice res', await res.json());
    })
    .catch(err => console.error('Error:', err))
  }

  render () {
    const { open } = this.state;

    return (
      <div>
        <Head>
          <title>IDM</title>
          <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh' crossOrigin='anonymous' />
        </Head>
        <nav className='nav d-flex p-2 bd-highlight justify-content-between'>
          <Link href='/'>
            <a className='navbar-brand text-info font-weight-bold'>IMÁGENES DE MUNDO</a>
          </Link>
          <Link href='/invoice'>
            <a className='nav-link text-info font-weight-bold'>FACTURAS</a>
          </Link>
          <button type='button' className='btn btn-outline-info font-weight-bold' onClick={this.onOpenModal}>Añadir Vendedor</button>
          <Modal open={open} onClose={this.onCloseModal} center>
            <div className='input-group p-5'>
              <input type='text' className='form-control' placeholder='Input a name' onChange={this.handleName} />
              <div className='input-group-append'>
                {this.state.name ? 
                  <button className='btn btn-outline-secondary' type='button' onClick={this.handleAddSeller}>Crear</button>
                  :
                  <button className='btn btn-outline-secondary' type='button'>Crear</button>
                }
              </div>
            </div>
          </Modal>
        </nav>
      </div>
    );
  }
}

export default Header;