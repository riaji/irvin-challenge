import withLayout from '../components/MyLayout';
import React, { Component } from 'react';
import fetch from 'isomorphic-unfetch';

const cardStyle = {
  width: '9rem'
};

const imgStyle = {
  maxWidth: '8rem',
  maxHeight: '8rem',
  width: 'auto',
  height: 'auto'
};

const extraCentered = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  justifyContent: 'center',
  alignItems: 'center'
};

class Race extends Component {
  constructor(props) {
    super(props);

    this.state = {
      keyword: null,
      liveImages: null,
      currentImages: null,
      sellers: null,
      race: null,
      winner: null,
      totalPoints: null,
      raceIsEnded: false,
      pagination: 1,
      imagesBUrl: 'https://www.googleapis.com/customsearch/v1?key=AIzaSyDV5WxK2waZ-h7VFxB3egdWLaOVgvVqH5E&cx=010845818822579063968:fyfmqbqbcp4&searchType=image',
      alegraBUrl: 'https://api.alegra.com/api/v1/',
      alegraToken: 'Basic aXJ2aW4uamFpci5wZ0BnbWFpbC5jb206MWRhZmJiN2I5ZTM4MmZjYTA3NDM=' 
    };

    this.randomImageAndRest = this.randomImageAndRest.bind(this);
    this.liveImagesAndCurrentImages = this.liveImagesAndCurrentImages.bind(this);
    this.handleKeyword = this.handleKeyword.bind(this);
    this.renderLiveSellers = this.renderLiveSellers.bind(this);
    this.createInvoice = this.createInvoice.bind(this);
    this.handleNewRace = this.handleNewRace.bind(this);
    this.getImages = this.getImages.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleMoreOpts = this.handleMoreOpts.bind(this);
    this.handleRaceAndWinner = this.handleRaceAndWinner.bind(this);
    this.handlePurchase = this.handlePurchase.bind(this);
  }
  
  randomImageAndRest (arr) {
    const randIdx = Math.round( Math.random() * (arr.length - 1) );
    const randomImage = arr[randIdx];
    const arrayRest = arr.filter((el, idx) => idx != randIdx);

    return {
      randomImage,
      arrayRest
    }
  }

  liveImagesAndCurrentImages (arr) {
    let currentImages = arr;
    const liveImages = [];
    const { sellers } = this.state;
    const numOfSellers = sellers.length;

    for (let i=0; i<numOfSellers; i++) {
      const { randomImage, arrayRest } = this.randomImageAndRest(currentImages);
      liveImages.push(Object.assign(
        randomImage, sellers[i]
      ));
      currentImages = arrayRest;
    }

    return {
      liveImages,
      currentImages
    }
  }

  async componentDidMount () {
    const url = window.location;
    const urlObject = new URL(url);
    const q = urlObject.searchParams.get('q');

    const getSellers = await fetch(this.state.alegraBUrl + 'sellers/', {
      headers:{
        'Authorization': this.state.alegraToken
      }
    }).catch(err => console.error(err));

    this.setState({
      keyword: q,
      sellers: await getSellers.json()
    });

    await this.getImages(this.state.imagesBUrl + `&q=${this.state.keyword}`)
    .then(() => {
      this.setState({
        race: this.state.sellers.map(el => {
          return { id: el.id, name: el.name, points: 0, restPoints: 20};
        })
      });
    })
    .catch(err => console.error(err));
  }

  async getImages(url) {
    await fetch(url, {
      method: 'GET'
    }).then(res => {
      return res.json();
    })
    .then(res => {
      const { liveImages, currentImages } = this.liveImagesAndCurrentImages(res.items);
      return { liveImages, currentImages };
    })
    .then(res => {
      this.setState({
        liveImages: res.liveImages,
        currentImages: res.currentImages
      });
    })
    .catch(err => console.error('Error:', err))
    
  }

  handleSearch () {
    this.setState({ pagination: 1 });
    this.getImages(this.state.imagesBUrl + `&q=${this.state.keyword}`);
    if (this.state.winner) {
      this.handleNewRace();
    }
    window.history.pushState('', '', `/race?q=${this.state.keyword}`);
  }

  handleMoreOpts () {
    const pag = this.state.pagination+1;
    this.setState({ pagination: pag });
    this.getImages(this.state.imagesBUrl + `&q=${this.state.keyword}&start=${1 + 10*(pag-1)}`)
  }

  async handleRaceAndWinner (id) {
    const updateRace = this.state.race.map(el => {
      if (el.id==id) {
        el.points = el.points + 3;
        el.restPoints = 20 - el.points;
      }
      return el;
    });

    let race = Object.assign(this.state.race, updateRace);
    race = race.sort((x, y) => (x.points < y.points) ? 1 : -1);

    this.setState({
      race: race
    });

    const winner = race.filter(el => el.points >= 20);

    if (winner.length) {
      const totalPoints = race.map(el => el.points).reduce((x,y) => x+y,0);
      
      this.setState({
        winner: await winner[0],
        totalPoints: await totalPoints,
        raceIsEnded: true
      });

      this.createInvoice();
    }
  }

  handlePurchase (id) {
    this.handleMoreOpts();
    this.handleRaceAndWinner(id);
  }

  handleKeyword (event) {
    event.preventDefault();
    this.setState({
      keyword: event.target.value
    });
  }

  handleNewRace () {
    this.setState({
      raceIsEnded: false,
      race: this.state.sellers.map(el => {
        return { id: el.id, name: el.name, points: 0, restPoints: 20 };
      })
    });
  }

  async createInvoice () {
    const { winner: id, totalPoints } = this.state;
    const invoice = {
      date: new Date().toJSON().split('T')[0],
      dueDate: new Date(new Date()*1 + 86400000).toJSON().split('T')[0],
      client:  1,
      seller: id,
      items: [
        {
          id: 1,
          price: totalPoints,
          quantity: 1,
          description: "WI race's WINNER"
        }
      ]
    }

    await fetch(this.state.alegraBUrl + 'invoices/1', {
      method: 'POST',
      body: JSON.stringify(invoice),
      headers:{
        'Content-Type': 'application/json',
        'Authorization': this.state.alegraToken
      }
    }).then(async res => {
      console.log('invoice res', await res.json());
    })
    .catch(error => console.error('Error:', error))
  }

  renderLiveSellers (sellers) {
    return [].concat(sellers).map( (seller, i) =>
      <div className='mb-5 text-center' style={cardStyle} key={seller.id}>
        <h5>{i+1}° Place</h5>
        {seller.points>=20 ? 
          <h2>{seller.name} <span className='badge badge-success'>{seller.points} &#8725; 20</span></h2> 
          : 
          <h2>{seller.name} <span className='badge badge-info'>{seller.points} &#8725; 20</span></h2>
        }
        {seller.points>=20 ? 
          <h6><span className='badge badge-primary'>W I N N E R</span></h6>
          : 
          <h6><span className='badge badge-warning'>{seller.restPoints} points to win</span></h6>
        }
      </div>
    );
  }

  renderLiveImages (images) {
    return [].concat(images).map( (img, i) =>
      <div className='card text-center m-3' key={i} style={cardStyle}>
        <figure className='figure' >
          <img src={img.image.thumbnailLink} className='card-img-top my-1' alt={img.title} style={imgStyle} />
          <figcaption className='figure-caption mx-2'>{img.title}</figcaption>
        </figure>
        <div className='card-body'>
          <h6 className='card-title'>By {img.name}</h6>
          <a className='btn btn-primary text-white' onClick={() => this.handlePurchase(i+1)} >Comprar</a>
        </div>
      </div>
    );
  }

  render () {
    return (
      <div>
        <div className='container'>
          <div className='row my-5'>
            <div className='col'>
              <div className='input-group mb-3 align-start'>
                <input type='text' className='form-control' placeholder='' onChange={this.handleKeyword} />
                <div className='input-group-prepend'>
                  {this.state.keyword ?
                    <button className='btn btn-outline-secondary' type='button' onClick={this.handleSearch} >Search</button>
                    :
                    <button className='btn btn-outline-secondary' type='button' >Search</button>
                  }
                </div>
              </div>
            </div>
          </div>

          <div style={extraCentered}>
            {this.state.race && this.renderLiveSellers(this.state.race)}
          </div>

          {this.state.raceIsEnded ? 
            <div className='row mb-5' style={extraCentered}>
              <button type='button' className='btn btn-success m-5' onClick={this.handleNewRace}>Nueva Carrera &gt;&gt;</button>
            </div>
            :
            <div className='row mb-5' style={extraCentered}>
              {this.state.liveImages && this.renderLiveImages(this.state.liveImages)}
              <button type='button' className='btn btn-success m-5' onClick={this.handleMoreOpts}>Quiero otras opciones &gt;&gt;</button>
            </div>
          }
        </div>
      </div>
    );
  }
}

export default withLayout(Race);