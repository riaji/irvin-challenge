import withLayout from '../components/MyLayout';
import React, { Component } from 'react';

const extraCentered = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  justifyContent: 'center',
  alignItems: 'center',
  margin: '1rem 0'
};

class Invoice extends Component {
  constructor(props) {
    super(props);

    this._isMounted = false;

    this.state = {
      invoices: null
    };

    this.renderInvoices = this.renderInvoices.bind(this);
  }

  async componentDidMount () {
    this._isMounted = true;
    this._isMounted && await fetch('https://api.alegra.com/api/v1/invoices/', {
      method: 'GET',
      headers:{
        'Authorization': 'Basic aXJ2aW4uamFpci5wZ0BnbWFpbC5jb206MWRhZmJiN2I5ZTM4MmZjYTA3NDM='
      }
    }).then(res => {
      return res.json();
    })
    .then(res => {
      return res.sort((x, y) => (x.points < y.points) ? 1 : -1);
    })
    .then(invoices => {
      this._isMounted = true && this.setState({
        invoices: invoices
      });
    })
    .catch(err => console.error('Error:', err))
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  renderInvoices (invoices) {
    return [].concat(invoices).map( (invoice, i) => 
      <div className='table-responsive px-5 mx-5 my-2' key={invoice.id}>
        <table className='table table-bordered table-sm'>
          <caption>Vendedor: {invoice.seller.name}</caption>
          <thead className='thead-dark'>
            <tr>
              <th scope='col'>ID</th>
              <th scope='col'>Cliente</th>
              <th scope='col'>Producto</th>
              <th scope='col'>descripción</th>
              <th scope='col'>Cantidad</th>
              <th scope='col'>Fecha</th>
            </tr>
          </thead>
          <tbody className='table-striped '>
            <tr className='table-info'>
              <th scope='row'>{invoice.id}</th>
              <td>{invoice.client.name}</td>
              <td>{invoice.items[0].name}</td>
              <td>{invoice.items[0].description}</td>
              <td>{invoice.total}</td>
              <td>{invoice.date}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }

  render () {
    return (
      <div style={extraCentered}>
        {this.state.invoices && this.renderInvoices(this.state.invoices)}
      </div>    
    );
  }
}

export default withLayout(Invoice);