import withLayout from '../components/MyLayout';
import React, { Component } from 'react';
import Link from 'next/link';

const HomeStyle = {
  display: 'flex',
  height: 'calc(95vh - 50px)',
  flexDirection: 'row',
  alignItems: 'center'
};

class Home extends Component {
  constructor (props) {
    super(props);

    this.state = {
      keyword: null
    };

    this.handleKeyword = this.handleKeyword.bind(this);
  }

  componentDidMount () {

  }

  handleKeyword (event) {
    event.preventDefault();
    this.setState({
      keyword: event.target.value
    });
  }

  render () {
    return (
      <div style={HomeStyle}>
        <div className='container'>
          <div className='row'>
            <div className='col text-center display-1 pb-5 text-info'>
              <h1>Imágenes de mundo</h1>
            </div>
          </div>
          <div className='row'>
            <div className='col'>
              <div className='input-group mb-3 align-middle'>
                <input type='text' className='form-control' placeholder='' onChange={this.handleKeyword} />
                <div className='input-group-prepend'>
                  {this.state.keyword ?
                    <Link href={{ pathname: '/race', query: { q: `${this.state.keyword}` } }}>
                      <button className='btn btn-outline-secondary' type='button' >Search</button>
                    </Link>
                    :
                    <button className='btn btn-outline-secondary' type='button' >Search</button>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withLayout(Home);